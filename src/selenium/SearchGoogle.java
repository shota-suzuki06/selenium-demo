package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchGoogle {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\env\\app\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.navigate().to("https://www.google.com/");
		driver.findElement(By.name("q")).sendKeys("Linux tutorials");
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		WebElement element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(
					By.name("btnK")
				)
		);
		element.click();
		
//		driver.close();
	}

}
